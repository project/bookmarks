<?php

namespace Drupal\bookmarks\Controller;

use Drupal\views\Views;
use Drupal\user\UserInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Returns responses for bookmarks module routes.
 */
class BookmarksController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @return string
   *   The page title.
   */
  public function title() {
    return $this->t('Bookmarks');
  }

  /**
   * Presents the list page.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user being viewed.
   *
   * @return array
   *   A form array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function build(UserInterface $user) {
    $account = $this->currentUser();
    $is_same = $user && ($account->id() == $user->id());
    if ($is_same || $account->hasPermission('access others bookmarks')) {
      $args = [];

      // Filter view by current user;
      $args[0] = $user->id();

      $build = [];
      if ($view = Views::getView('bookmarks')) {
        $view->setArguments($args);
        $view->execute();
        $build['list'] = $view->render('default');
        $build['list']['#pre_render'][] = [\Drupal::service('bookmarks.manager'), 'postRenderList'];
      } else {
        $build['list'] = ['#markup' => $this->t('Missing view: bookmarks')];
      }

      return $build;
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }
}
