<?php

namespace Drupal\bookmarks\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Overrides Flagging admin_permission to support flag/unflag bookmark edit access.
 */
class BookmarksAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\flag\FlaggingInterface $entity */
    $flag = $entity->getFlag('bookmark');
    $action = $flag->isFlagged($entity) ? 'unflag' : 'flag';
    return parent::checkAccess($entity, $operation, $account)
      ->orIf($flag->actionAccess($action, $account));
  }

}
